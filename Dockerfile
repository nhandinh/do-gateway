FROM openjdk
WORKDIR /usr/src/app
COPY build/libs/do-gateway-1.0-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/src/app/app.jar"]

