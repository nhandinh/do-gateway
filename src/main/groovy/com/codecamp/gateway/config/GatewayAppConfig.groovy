package com.codecamp.gateway.config

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Create by dhnhan on 12/30/2019
 */

@ConfigurationProperties(prefix = 'app')
class GatewayAppConfig {

    boolean securityApi
}
