package com.codecamp.gateway.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.springframework.web.servlet.resource.GzipResourceResolver
import org.springframework.web.servlet.resource.PathResourceResolver

@Configuration
@EnableWebMvc
class MvcConfig implements WebMvcConfigurer {

  @Override
  void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry
        .addResourceHandler("/media/**")
        .addResourceLocations('file:/var/lib/do-media/')
        .setCachePeriod(3600)
        .resourceChain(true)
        .addResolver(new GzipResourceResolver())
        .addResolver(new PathResourceResolver())
    registry
        .addResourceHandler("/assets/**")
        .addResourceLocations('classpath:/templates/')
        .setCachePeriod(3600)
        .resourceChain(true)
        .addResolver(new GzipResourceResolver())
        .addResolver(new PathResourceResolver())
  }

}
