package com.codecamp.gateway

import com.codecamp.gateway.config.GatewayAppConfig
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.cloud.netflix.eureka.EnableEurekaClient
import org.springframework.cloud.netflix.zuul.EnableZuulProxy

@EnableZuulProxy
@SpringBootApplication
@EnableEurekaClient
@EnableConfigurationProperties(GatewayAppConfig.class)
class GatewayApplication {

  static void main(String[] args) {
    SpringApplication.run GatewayApplication, args
  }
}
