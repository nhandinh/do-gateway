package com.codecamp.gateway.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class IndexController {

  @RequestMapping(value = '/app/**')
  String index() {
    return 'index'
  }

  @RequestMapping(value = '/')
  String appHome() {
    'redirect:/app'
  }
}
