package com.codecamp.gateway.filter

import com.codecamp.gateway.config.GatewayAppConfig
import com.netflix.zuul.ZuulFilter
import com.netflix.zuul.context.RequestContext
import com.netflix.zuul.exception.ZuulException
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.servlet.http.HttpServletRequest

@Slf4j
@Component
class AuditLoggerFilter extends ZuulFilter {

    @Autowired
    GatewayAppConfig appConfig

    @Override
    String filterType() {
        return "pre"
    }

    @Override
    int filterOrder() {
        return 1
    }

    @Override
    boolean shouldFilter() {
        appConfig.securityApi
    }

    @Override
    Object run() throws ZuulException {
        RequestContext ctx = RequestContext.getCurrentContext()
        HttpServletRequest request = ctx.getRequest()
        String uri = request.requestURI
        String method = request.method
        log.info("Request info: [${method}] - ${uri}")
        return null
    }
}
